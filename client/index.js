import React, { Component } from 'react'
import { browserHistory } from 'dva/router';
import { BrowserRouter } from 'dva/router';
import Router from 'common/router';
import createApp from 'common/createApp'
import models from './models';
import createHistory from 'history/createBrowserHistory';
import { AppContainer } from 'react-hot-loader'
const app = createApp({
  history: createHistory()
})

app.router(({}) => {
  return (
    <BrowserRouter
      history={browserHistory}
      forceRefresh={true}>
      <Router />
    </BrowserRouter>
  )
})
app.start('#root')