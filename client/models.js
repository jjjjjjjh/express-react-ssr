import Index from './index/model'

export default (app) => {
  app.model(Object.assign({}, Index))
}