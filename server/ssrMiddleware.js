import { StaticRouter } from 'dva/router';
import { Provider } from 'react-redux'
import { createMemoryHistory } from 'history';
import ReactDOMServer from 'react-dom/server'
import React, { Component } from 'react'
import createApp from '../common/createApp'

export default (req, res) => {
  const context = {}
  const initialState = {}
  const app = createApp({
    history: createMemoryHistory(),
  }, req, context)

  const html = (()=>{
    if(process.env.NODE_ENV === 'production') { // 如果是生产环境采用服务器端渲染
      return ReactDOMServer.renderToString(
        app.start()()
      )
    }
    return ''
  })()
  res.render('layout', {html: html})
}