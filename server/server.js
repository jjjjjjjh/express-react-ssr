import express from 'express';
import webpack from 'webpack';
import config from '../build/config';
import webpackConfig from '../build/webpack.config.babel'
import path from 'path';
const app = express()

app.set('views', __dirname + '/views');
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

// 开发环境测试环境
if(process.env.NODE_ENV !== 'production') {
  const compiler = webpack(webpackConfig())
  const options = {
    publicPath: config.publicPath,
    noInfo: true,
    hot: true,
    stats: { color: true }
  }
  app.use(require('webpack-dev-middleware')(compiler, options));
  app.use(require('webpack-hot-middleware')(compiler));
}

app.use('/public', express.static(path.resolve(__dirname, '../public')));
app.use(require('./ssrMiddleware').default);
app.disable('x-powered-by')

const server = app.listen(config.port, () => {
  const {port} = server.address()
  console.log(`Listened at http://localhost:${port}`)
})