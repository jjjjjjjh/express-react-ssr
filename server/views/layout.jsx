import React, { Component } from 'react'
class Layout extends Component {
  render() {
    return (
      <html>
        <head>
          <meta charSet="utf-8" />
        </head>
        <body>
          <div id="root" dangerouslySetInnerHTML={{__html: this.props.html}}>
          </div>
          <script src="/public/js/vendor_dll.js"></script>
          <script src="/public/js/index.js"></script>
        </body>
      </html>
    )
  }
}
module.exports = Layout