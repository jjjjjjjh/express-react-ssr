const webpack = require('webpack');
const path = require('path')
const config = require('./config')
module.exports = {
  output: {
    path: path.resolve(__dirname, "../public/js"),
    filename: '[name]_dll.js',
    library: '[name]_[chunkhash]',
  },
  entry: {
    vendor: config.dll,
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.resolve(__dirname, '../public/js/dll/[name]-manifest.json'),
      name: '[name]_[chunkhash]',
    }),
  ],
};