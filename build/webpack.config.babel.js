import { resolve } from 'path';
import merge from 'webpack-merge';
import prodConfig from './webpack.config.prod'
import devConfig from './webpack.config.dev'
import CleanWebpackPlugin from 'clean-webpack-plugin';
import HtmlWebPackPlugin from "html-webpack-plugin";
import webpack from 'webpack';
import config from './config';

const baseConfig = {
  output: {
    filename: "[name].js",
    path: resolve(__dirname, "../public/js"),
    publicPath: config.publicPath,
    globalObject: 'this'
  },
  resolve: {
    extensions: [".js", ".json", ".jsx", ".less"],
    alias: {
      client: resolve(__dirname, '../client'),
      common: resolve(__dirname, '../common')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        }
      },
      {
        test: /\.less$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'less-loader'
        }]
      },
      {
        test: /\.gif|.png|.jpg$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              outputPath:'/img'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin('../public/js'),
    new webpack.DllReferencePlugin({
      manifest: resolve(__dirname, '../public/js/dll/vendor-manifest.json'),
    }),
  ]
}

module.exports = env => {
  if (env && env.production) {
    return merge(baseConfig, prodConfig)
  }
  return merge(baseConfig, devConfig)
}