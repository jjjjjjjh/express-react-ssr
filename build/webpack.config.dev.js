import path from 'path'
import { resolve } from 'path';
import OpenBrowserPlugin from 'open-browser-webpack-plugin'
import webpack from 'webpack'
import config from './config'

module.exports = {
  entry: {
    index: ['webpack-hot-middleware/client?reload=true', resolve(__dirname, '../client/index.js')],
    vendor: ['react', 'react-dom', 'dva', 'lodash']
  },
  mode: 'development',
  devtool: 'source-map',
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          query: {
            presets: ['react-hmre']
          }
        }
      },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new OpenBrowserPlugin({ url: `http://localhost:${config.port}` }),

  ]
}