import webpack from 'webpack';
import { resolve } from 'path';

module.exports = {
  mode: 'production',
  entry: {
    index: [resolve(__dirname, '../client/index.js')],
    vendor: ['react', 'react-dom', 'dva', 'lodash']
  },
  optimization: {
    minimize:true,
    concatenateModules: true,
  },
}