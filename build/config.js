module.exports = {
  port: 8001,
  publicPath: '/public/js',
  dll: [
    'react',
    'react-dom',
    'react-redux',
    'react-router',
    'redux',
    'dva',
  ]
}