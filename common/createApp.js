import dva from 'dva';
import React from 'react';
import { RouterContext, StaticRouter } from 'dva/router';
import Router from '../common/router';
import models from '../client/models'
export default function createApp(opts, req, context) {
  const app = dva(opts);
  // 挂载models
  models(app)
  app.router(() => {
    return (
      <StaticRouter
        location={req.url}
        context={context}
      >
        <Router />
      </StaticRouter>
    )
  })
  return app
}