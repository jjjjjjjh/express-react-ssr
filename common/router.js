import React from 'react';
import { Switch, RedirectWithStatus, Route } from 'dva/router';
import Index from '../client/index/index'
export default () => (
  <Switch>
    <Route path="/" component={Index} />
  </Switch>
)