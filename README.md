
# 服务器端渲染脚手架

## 特性

1. 支持服务器端路由渲染,页面静态输出

2. 开发模式热更新数据替换

## 采用框架

1. 数据管理使用dva

2. 服务器端渲染采用express

## 项目结构

```
|____.babelrc                   // babel转换规则
|____server                     // 存放服务器端代码
| |____server.js                // 服务器端配置,及其启动服务器
| |____index.js                 // 服务器注册
| |____views                    // 服务器端视图目录
| | |____layout.jsx             // layout
| |____ssrMiddleware.js         // 进行服务器端渲染中间件
|____README.md                  // 服务端
|____common                     // 服务器端和客户端可共用部分
| |____createApp.js             // 创建服务器端静态渲染
| |____router.js                // 客户端服务器端共用路由
|____public                     // 静态资源
| |____js
| |____img
|____package-lock.json
|____package.json               // 项目配置与项目启动配置
|____build                      // 项目配置
| |____webpack.config.prod.js   // 生成环境项目配置参数
| |____config.js                // 通用配置
| |____webpack.config.babel.js  // 客户端编译基本配置
| |____webpack.config.dev.js    // 开发环境基础配置
| |____webpack.config.vendor.js // dll生成
|____client                     // 客户端页面存放
| |____index.js                 // 客户端启动配置
| |____models.js                // dva model数据注册
| |____index                    // index 页面
| | |____index.js
| | |____model.js

```

## 注意事项

服务器端渲染不支持`require`(jpg,gif,svg,png) 等非js脚本的支持

如静态文件需要部署在`public`资源文件里,页面引用

```
  <img src="/public/img/1.jpg" />
```

## 流程执行图

![logic.png](logic.png)